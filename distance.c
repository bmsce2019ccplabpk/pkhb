#include<stdio.h>
#include<math.h>
int main()
{
   float distance(float,float,float,float);
   float x1,y1,x2,y2;
   float res;
   printf("Enter the coordinates of first point\n");
   scanf("%f %f",&x1,&y1);
   printf("Enter the coordinates of second point\n");
   scanf("%f %f",&x2,&y2);
   res=distance(x1,y1,x2,y2);
   printf("Distance Between The Two Points:%.3f\n",res);
   return 0;
}
float distance(float x1,float y1,float x2,float y2)
{
    float d;
    d=sqrt(pow((x2-x1),2)+pow((y2-y1),2));
    return(d);
}