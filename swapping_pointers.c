#include<stdio.h>
int main()
{
   void swap(int *,int *);
   int a=10,b=20;
   printf("Before Swapping:a=%d\tb=%d\n",a,b);
   swap(&a,&b);
   printf("After  Swapping:a=%d\tb=%d\n",a,b);
   return 0;
}
void swap(int *x,int *y)
{
   int temp;
   temp=*x;
   *x=*y;
   *y=temp;
}